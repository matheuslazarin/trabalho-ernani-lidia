const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const instituicao = require('./routes/instituicao');
const port = 3001;
const allowedOrigins = ['http://localhost:3001/', 'http://localhost:3000/'];
app.use(cors({ 
  origin: (origin, callback)=>{
    if(!origin) return callback(null, true);
    /*if(allowedOrigins.indexOf(origin) === -1){
      var msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }*/
    return callback(null, true);
  },
 // credentials: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use('/api/instituicao', instituicao);
app.get('/api', (req, res)=>{
    res.send('Running');
});

app.listen(port, ()=>{
    console.log('fodase');
});