var Mongoose = require('./index').mongoose;

var unidadeShema = Mongoose.Schema({
    nome: {type: String, required: true, unique: true}
});

var instituicaoSchema = Mongoose.Schema({
    nome: {type: String, required: true, unique: true},
    unidades:[unidadeShema]
});

var Instituicao = Mongoose.model('Instituicao', instituicaoSchema);

const crud = {
    insert: (req, res)=>{
        var instituicao = new Instituicao(req.body);
        instituicao.save((error, result)=>{
            res.json(result);
        });
    },
    getAll: (res)=>{
        Instituicao.find({}, (error, result)=>{
            if(error) res.json(error);
            res.json(result);
        });
    },
    getById: (id, res)=>{
        Instituicao.findById(id, (error, inst)=>{
            if(error) res.json(error);
            res.json(inst);
        });
    },
    update: (req, res)=>{
        Instituicao.findByIdAndUpdate(req.params.id, req.body, (error, result)=>{
            if(error) res.json(error);
            res.json(result);
        });
    },
    delete: (id, res)=>{
        Instituicao.findByIdAndDelete(id, (error, result)=>{
            if(error) res.json(error);
            res.json(result);
        });
    }
}

module.exports = crud;