const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/observatorio',{ useNewUrlParser: true });
var db = mongoose.connection;

db.on('error', console.error);
db.once('open', function () {
    console.log('Conectado ao MongoDB.')
    // Vamos adicionar nossos Esquemas, Modelos e consultas aqui
});
const database = {};
database.mongoose = mongoose;
database.db = db;
module.exports = database;
