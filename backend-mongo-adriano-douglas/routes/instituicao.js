var express = require('express');
var Instituicao = require('../models/instituicao');
var router = express.Router();

router.post('/', (req, res)=>{
    Instituicao.insert(req, res);
});
router.get('/', (req, res)=>{
    Instituicao.getAll(res);
});
router.get('/:id', (req, res)=>{
    Instituicao.getById(req.params.id, res);
});
router.delete('/:id', (req, res)=>{
    Instituicao.delete(req.params.id, res);
});
router.put('/:id', (req, res)=>{
    Instituicao.update(req, res);
});
module.exports = router;